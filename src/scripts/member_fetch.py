import argparse
import pandas
import requests
from io import StringIO


def fetch(args):
    headers = {
        'Authorization': 'Bearer ' + args.token
    }
    req = requests.get(
        args.url + '/api/v4/groups/' + args.gid + '/members',
        headers=headers
    )
    print(convert(req.text))


def convert(json_data):
    list = pandas.read_json(StringIO(json_data), encoding='utf-8')
    return list.to_csv()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help='Target URL of Gitlab repository')
    parser.add_argument('gid', help='Target group ID')
    parser.add_argument('token', help='CI bearer token')
    fetch(parser.parse_args())
