# Sample Markdown
技術文書のサンプルです．CI/CD機能でtextlintをかけ，PDF化するところまでをテストしたい．

```c
#include <stdio.h>

int main()
{
    printf("Hello, World!\n");

    return 0;
}
```

```ruby
puts "Hello, World!" #=> "Hello, World!"
```

```python
print("Hello, World!")
```

## lint
`textlint`及び各種プリセットを用いたlintingにより，技術文書として正しい書き方を意識させることを可能にします．正しく技術を伝えるためには，精緻な日本語を用いることが望ましいです．lintの設定は `.textlintrc` を参照してください．
